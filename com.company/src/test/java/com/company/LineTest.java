package com.company;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LineTest {



	@Test
	public void testIntersection1() {
		Line line1 = new Line(1, 3);
		Line line2 = new Line(1, 4);

		assertTrue(line1.hasIntersection(line2));
	}

	@Test
	public void testIntersection2() {
		Line line1 = new Line(1, 3);
		Line line2 = new Line(1, 6);

		assertTrue(line1.hasIntersection(line2));
	}

	@Test
	public void testIntersection3() {
		Line line1 = new Line(1, 3);
		Line line2 = new Line(1, 10);

		assertTrue(line1.hasIntersection(line2));
	}

	@Test
	public void testIntersection4() {
		Line line1 = new Line(1, 3);
		Line line2 = new Line(4, 2);

		assertFalse(line1.hasIntersection(line2));
	}

	@Test
	public void testIntersection5() {
		Line line1 = new Line(4, 2);
		Line line2 = new Line(5, 5);

		assertTrue(line1.hasIntersection(line2));
	}
	
}
