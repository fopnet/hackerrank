package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Chat {

	static final String THREAD_SEPARATOR = "---";

	public String user1;
	public String user2;
	public String message;

	public Chat(String user1, String user2, String message) {
		this.user1 = user1.trim();
		this.user2 = user2.trim();
		this.message = message.trim();
	}

	public String getThreadId() {
		String first = user1.compareToIgnoreCase(user2) > 0 ? user2.toLowerCase().trim()
				: user1.toLowerCase().trim();
		String last = user1.compareToIgnoreCase(user2) > 0 ? user1.toLowerCase().trim()
				: user2.toLowerCase().trim();

		return first.concat(last).concat(this.getThread().trim());
	}

	public int getThreadNumber() {
		String regex = THREAD_SEPARATOR;

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(this.message);

		int count = 0;
		while (matcher.find()) {
			count++;
			// System.out.println("found: " + count + " : " + matcher.start() + " - " +
			// matcher.end());
		}

		return count;
	}

	public String getThread() {
		int idx = this.message.lastIndexOf(THREAD_SEPARATOR);
		String result = null;

		if (idx == -1) {
			result = this.message;

		} else {

			result = this.message.substring(idx + THREAD_SEPARATOR.length());
		}

		return result.toLowerCase().trim();
	}
}