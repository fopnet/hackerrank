package com.company;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * While creating a super awesome email cliente, the administrator encounters a problem: 
 * Hot to keep track of email threads ? The established methods of matching subject line or tracking a unique identifier seemed unreliable, so there is a new approach. 
 * The email body is compared to previously mails and if there is a match, it will be market as part of the thread. 
 * 
 * Emails are given in order they were sent, and each email contains 3 fields: the sender, the receiver and the mail body. 
 * A thread will always consists of the same two people and replies will be in format as described earlier.
 * 
 * 
 * Take for example n = 3 emails given by = 
 * [
 * 	('john@gmail.com', 'susan@gmail.com', 'Are you back from vacation?'), 
 * 	('bob@gmail.com', 'alice@gmail.com', 'did you get the key?'), 
 * 	('suzan@gmail.com, 'john@gmail.com', 'Just got in, ---Are you back from vacition?')
 * ] 
 * 
 * There are two threads in the emails. 
 * The return array should be two dimensional array where each element consists of two integers: the email thread id and the position within the thread, 
 * so in this case, the output sould be [ (1,1), (2,1), (1,2) ]. 
 * 
 * This signifies that the first email is in thread 1, and its position is 1, 
 * the second email is in thread 2 and its position in that thread is 1, 
 * and the thrird email is in the thread 1, position 2. 
 * 
 * @author Felipe
 *
 */

public class EmailThreadTest {

	@Test
	public void testEmailThread() {

		List<String> chat1 = new ArrayList<String>(
				Arrays.asList("fop.net@gmail.com", "fop.net@bol.com.br", "hello x, how are you?"));

		List<String> chat2 = new ArrayList<String>(Arrays.asList("fop.net@gmail.com",
				"contato@email.com", "did you take a look at the event?"));

		List<String> chat3 = new ArrayList<String>(Arrays.asList("fop.net@bol.com.br",
				"fop.net@gmail.com", "i am great. how are you?---hello x, how are you?"));

		List<List<Integer>> result = this.getConversations(chat2.size(),
				new ArrayList<List<String>>(Arrays.asList(chat1, chat2, chat3)));

		System.out.println("testEmailThread => " + result);
	}

	@Test
	public void testEmailThread2() {

		List<String> chat1 =
				new ArrayList<String>(Arrays.asList("sv@outlook.com", "gfdpdyf@gmail.com", "n"));

		List<String> chat2 =
				new ArrayList<String>(Arrays.asList("tk@outlook.com", "wl@hackerrank.com", "m"));

		List<String> chat3 = new ArrayList<String>(
				Arrays.asList("gfdpdyf@gmail.com", "sv@outlook.com", "t.xg---n"));

		List<String> chat4 = new ArrayList<String>(
				Arrays.asList("sv@outlook.com", "gfdpdyf@gmail.com", "i---t.xg---n"));

		List<String> chat5 = new ArrayList<String>(
				Arrays.asList("tk@outlook.com", "wl@hackerrank.com", "o---m"));

		List<String> chat6 = new ArrayList<String>(
				Arrays.asList("tk@outlook.com", "wl@hackerrank.com", "w---o---m"));

		List<String> chat7 = new ArrayList<String>(
				Arrays.asList("tk@outlook.com", "wl@hackerrank.com", "nv---w---o---m"));

		List<String> chat8 = new ArrayList<String>(
				Arrays.asList("sv@outlook.com", "gfdpdyf@gmail.com", "cyhf---i---t.xg---n"));

		List<List<Integer>> result =
				this.getConversations(chat2.size(), new ArrayList<List<String>>(
						Arrays.asList(chat1, chat2, chat3, chat4, chat5, chat6, chat7, chat8)));

		List<Integer> a1 = Arrays.asList(1,1);
		List<Integer> a2 = Arrays.asList(2,1);
		List<Integer> a3 = Arrays.asList(1,2);
		List<Integer> a4 = Arrays.asList(1,3);
		List<Integer> a5 = Arrays.asList(2,2);
		List<Integer> a6 = Arrays.asList(2,3);
		List<Integer> a7 = Arrays.asList(2,4);
		List<Integer> a8 = Arrays.asList(1,4);

		System.out.println("testEmailThread => " + result);
		
		assertEquals(Arrays.asList(a1, a2, a3, a4, a5, a6, a7, a8), result);
		
	}
	
	@Test
	public void testEmailSampleCase1() {

		String s1  = "abc@gmail.com, x@gmail.com, hello x, how are you?";
		String s2 = "c@gmail.com, abc@gmail.com, did you take a look at the event?";
		String s3 = "x@gmail.com, abc@gmail.com, i am great. how are you?---hello x, how are you?";
		
		List<Integer> a1 = Arrays.asList(1,1);
		List<Integer> a2 = Arrays.asList(2,1);
		List<Integer> a3 = Arrays.asList(1,2);

		List<List<Integer>> result = this.getEmailThreads2(
				new ArrayList<String>(Arrays.asList(s1, s2, s3)));

		assertEquals(Arrays.asList(a1, a2, a3), result);
		
		System.out.println("testEmailThread 3=> " + result);
	}

	public List<List<Integer>> getConversations(int size, List<List<String>> inputList) {
		List<Chat> chats = new ArrayList<Chat>();
		for (List<String> inputs : inputList) {
			chats.add(new Chat(inputs.get(0), inputs.get(1), inputs.get(2)));
		}

		return getEmailThreads(chats);

	}

	/**
	 * Overload of EmailThread receiving as parameter a modeling chat list
	 * 
	 * @param chats
	 * @return
	 */
	public List<List<Integer>> getEmailThreads(List<Chat> chats) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		// int threads = 0;

		for (Chat chat : chats) {
			int matches = chat.getThreadNumber();

			// System.out.println("matches " + matches);

			Integer threadNumber = map.get(chat.getThreadId());

			if (matches == 0) {

				if (threadNumber == null) {
					threadNumber = map.keySet().size() + 1;
				}

				// System.out.println("threadNumber " + threadNumber);
				// System.out.println("threadId " + chat.getThreadId());
				//
				map.put(chat.getThreadId(), threadNumber);

				res.add(new ArrayList<Integer>(Arrays.asList(threadNumber, 1)));

				// System.out.println(threadNumber + ", 1");
			} else {

				String threadId = chat.getThreadId();

				Integer emailNumber = matches + 1;

				res.add(new ArrayList<Integer>(Arrays.asList(map.get(threadId), emailNumber)));

				// System.out.println(map.get(threadId) + ", " + emailNumber);
			}
		}

		return res;

	}
	

	/**
	 * Returns the email threads receiving the emails as a String  List
	 * 
	 * @param emails
	 * @return
	 */
	public List<List<Integer>> getEmailThreads2(List<String> emails) {
	    // Write your code here

	        List<Chat> chats = new ArrayList<Chat>();
	        for (String inputs : emails) {
	            String[] splits = inputs.split(",");
	            System.out.println("u1 =>" + splits[0]);
	            System.out.println("u2 =>" + splits[1]);
	            String msg = String.join("", Arrays.copyOfRange(splits, 2, splits.length)).trim();
	            System.out.println("msg =>" + msg);
	            chats.add(new Chat(splits[0], splits[1], msg));
	        }

	        List<List<Integer>> res = new ArrayList<List<Integer>>();
	        Map<String, Integer> map = new HashMap<String, Integer>();
	        // int threads = 0;

	        for (Chat chat : chats) {
	            int matches = chat.getThreadNumber();

	            // System.out.println("matches " + matches);

	            Integer threadNumber = map.get(chat.getThreadId());

	            if (matches == 0) {

	                if (threadNumber == null) {
	                    threadNumber = map.keySet().size() + 1;
	                }

	                // System.out.println("threadNumber " + threadNumber);
	                // System.out.println("threadId " + chat.getThreadId());
	                //
	                map.put(chat.getThreadId(), threadNumber);

	                res.add(new ArrayList<Integer>(Arrays.asList(threadNumber, 1)));

	                // System.out.println(threadNumber + ", 1");
	            } else {

	                String threadId = chat.getThreadId();

	                Integer emailNumber = matches + 1;

	                res.add(new ArrayList<Integer>(Arrays.asList(map.get(threadId), emailNumber)));

	                // System.out.println(map.get(threadId) + ", " + emailNumber);
	            }
	        }

	        return res;

	    }

	
}
