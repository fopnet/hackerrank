package com.company;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * O objetivo é retornar o máximo de eventos possíveis que caibam em 1 dia.
 * 
 * A modelagem é : 
 * 	primeiro array significa hora início do evento
 *  segundo: indica a duração que o evento terá.
 *  
 *  É preciso retornar quantos possíveis eventos são possíveis ser agendados de forma que não haja interseções. 
 * 
 * @author Felipe
 *
 */
public class MaxEventTest {
	
	@Test
	public void testMaxEvent1() {

		assertEquals(2, this.maxEvents(new ArrayList<Integer>(Arrays.asList(5, 1, 1, 1, 1, 4)),
				new ArrayList<Integer>(Arrays.asList(5, 10, 3, 6, 4, 2))));

	}

	@Test
	public void testMaxEvent2() {

		assertEquals(4, this.maxEvents(Arrays.asList(1, 3, 3, 5, 7), Arrays.asList(2, 2, 1, 2, 1)));

	}

	@Test
	public void testMaxEvent3() {

		assertEquals(3, this.maxEvents(Arrays.asList(3, 1, 3, 5), Arrays.asList(3, 2, 2, 2)));

	}
	
	@Test
	public void testMaxEvent4() {
		
		assertEquals(1, this.maxEvents(Arrays.asList(1, 1), Arrays.asList(1, 5)));
		
	}
	
	@Test
	public void testMaxEventSampleCase0() {
		
		assertEquals(3, this.maxEvents(Arrays.asList(1, 3, 5), Arrays.asList(2, 2, 2)));
		
	}

	@Test
	public void testMaxEventSampleCase1() {

		assertEquals(1, this.maxEvents(Arrays.asList(1), Arrays.asList(5)));

	}
	
	public int maxEvents(List<Integer> arrivals, List<Integer> durations) {

		int eventNumbers = 0;
		List<Line> lines = new ArrayList<Line>();
		for (int curr = 0; curr < arrivals.size(); curr++) {
			lines.add(new Line(arrivals.get(curr), durations.get(curr)));
			if (lines.get(curr).isFitOneDay()) {
				eventNumbers = 1;
			}
		}

		/// sorting by arrivals and shorter durations
		Collections.sort(lines);
		for (int curr = 0; curr < lines.size() - 1;) {
			boolean hasIntersection = lines.get(curr).hasIntersection(lines.get(curr + 1));

			if (hasIntersection) {
				Line removed = lines.remove(curr + 1);
				System.out.println("removing " + removed.toString());
			} else {
				// System.out.println("has fit");
				eventNumbers++;
				curr++;
			}
		}

		System.out.println(lines);

		return eventNumbers;

	}

}
