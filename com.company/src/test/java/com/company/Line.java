package com.company;

public class Line implements Comparable<Line> {

	public Integer arrival;
	public Integer duration;

	public Line(Integer arrival, Integer duration) {
		this.arrival = arrival;
		this.duration = duration;
	}

	@Override
	public int compareTo(Line line) {
		if (this == line || (this.arrival == line.arrival && this.duration == line.duration)) {
			return 0;
		}
		if (this.arrival >= line.arrival) {

			if (this.arrival > line.arrival)
				return 1;
			else if (this.arrival == line.arrival && this.duration > line.duration)
				return 1;
			else
				return -1;

		} else {
			return -1;
		}
	}

	boolean hasIntersection(Line line) {

		if (line.arrival >= this.arrival && line.arrival < this.arrival + this.duration) {
			// System.out.println("if 1");
			return true;
		} else if (this.arrival >= line.arrival
				&& this.arrival < line.arrival + line.duration) {
			// System.out.println("if 2");
			return true;
		} else {
			// System.out.println("else if");
			return false;
		}

	}

	public boolean isFitOneDay() {
		return this.arrival + this.duration < 24;
	}

	public String toString() {
		return "arrival " + this.arrival + " duration " + this.duration;
	}
}