package com.company;

public class Student {

		public int direction;
		public int time;
		public int arrayIndex;

		public Student(int time, int direction, int arrayIdx) {
			this.time = time;
			this.direction = direction;
			this.arrayIndex = arrayIdx;
		}

		public int getTimeToLeave() {
			return this.time;
		}

		private void waitOneSecond() {
			this.time++;
		}

		public Student getWinner(Student next, Student previous) {
			Student winner = null;

			if (next == null)
				return this;

			if (this.time == next.time) {
//				System.out.println("this.time == next.time");

				if (previous == null || previous.isGetOut()) {
					winner = this.getGetOut(next);
//					System.out.println("Winner is getOut");
				} else {
					winner = this.getGetIn(next);
//					System.out.println("Winner is getIn");
				}

				if (winner.equals(this))
					next.waitOneSecond();
				else
					this.waitOneSecond();

			} else if (this.time < next.time) {
				return this;
			} else {
				return next;
			}

			return winner;
		}

		@Override
		public boolean equals(Object p) {
			if (p == null)
				return false;
			if (p == this)
				return true;
			if (!(p instanceof Student))
				return false;
			return ((Student) p).arrayIndex == this.arrayIndex;
		}

		@Override
		public int hashCode() {
			return arrayIndex;
		}

		boolean isGetIn() {
			return this.direction == 0;
		}

		boolean isGetOut() {
			return this.direction == 1;
		}

		Student getGetOut(Student next) {
			if (this.isGetOut() && next.isGetIn()) {
				return this;
			} else if (this.isGetIn() && next.isGetOut()) {
				return next;
			} else {
				return this;
			}
		}

		Student getGetIn(Student next) {
			if (this.isGetOut() && next.isGetIn()) {
				return next;
			} else if (this.isGetIn() && next.isGetOut()) {
				return this;
			} else {
				return this;
			}
		}

		public String toString() {
			return "Person " + (this.arrayIndex + 1);
		}

	}