package com.company;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;


/**
 * 
 * Uma faculdade possiu uma roleto e duas filas, uma para entrada de alunos, e outra para saída de alunos.
 * 
 * O primeiro parâmetro simboliza o segundo (s) que o estudante chega na roleta
 * o segundo parâmetro simboliza a direção que o estudante que passar na roleta
 * 
 * Regras:
 * 1. Se o momento anterior a roleta não foi usada, a preferência é de quem quer sair da universidade.
 * 2. Se o momento anterior algum estudante saiu, a preferência é de quem sai.
 * 3. Se o momento anterior algum estudante entrou, a preferência é de quem entra.
 *
 * 
 * Resultado esperado é uma lista de inteiros com o momento em que cada estudante passou na roleta.
 *
 *  Exemplo: 
 *  [ 0. 0, 1, 5 ] -> O primeiro estudante tentou entrar no segundo 0
 *  
 *  Direções onde: 
 *  0 : entrar
 *  1 : sair
 *  
 *  [ 0, 1, 1, 0 ] -> o primeiro estudante quer entrar.
 *  
 *  Resultado esperado
 *  
 *  [2, 0, 1, 5] -> O primeiro estudante passou no segundo 2(s), o segundo passou no primeiro momento 0s, o terceiro passou em 1(s), e o quarto entrou no segundo 5(s).
 *  
 *  		
 * @author Felipe
 *
 */
public class TurnstileTest {

	@Test
	public void testGetTimesSampleCase0() {
		List<Integer> times = Arrays.asList(0, 0, 1, 5);
		List<Integer> directions = Arrays.asList(0, 1, 1, 0);
		
		List<Integer> result = this.getTimes(times, directions);
		
		assertEquals(Arrays.asList(2, 0, 1, 5), result);
	}

	@Test
	public void testGetTimes2() {
		List<Integer> times = Arrays.asList(0, 1, 3, 5);
		List<Integer> directions = Arrays.asList(0, 1, 1, 0);
		
		List<Integer> result = this.getTimes(times, directions);
		
		assertEquals(Arrays.asList(0, 1, 3, 5), result);
	}
	
	@Test
	public void testGetTimesSampeCase1() {

		
		List<Integer> times = Arrays.asList(0, 1, 1, 3, 3);
		List<Integer> directions = Arrays.asList(0, 1, 0, 0, 1);

		List<Integer> result = this.getTimes(times, directions);

		assertEquals(Arrays.asList(0, 2, 1, 4, 3), result);
	}

	private List<Integer> getTimes(List<Integer> times, List<Integer> directions) {
		LinkedList<Student> persons = new LinkedList<Student>();

		Integer[] initArray = new Integer[times.size()];
		Arrays.fill(initArray, 0);
		
		List<Integer> timeResults = new ArrayList<Integer>(Arrays.asList(initArray));
		System.out.println("Init values array " + timeResults);
		

		for (int curr = 0; curr < times.size(); curr++) {
			Student p = new Student(times.get(curr), directions.get(curr), curr);
			persons.add(p);
		}

		Student previous = null;
		while (!persons.isEmpty()) {
			Student first = persons.getFirst();
			Student next = persons.size() == 1 ? null : persons.get(1);

			Student winner = first.getWinner(next, previous);

			System.out.println(" Winner is " + winner.toString());

			if (persons.remove(winner)) {
				previous = winner;
				System.out.println("Revoving winner " + winner.toString());
			}

			timeResults.set(winner.arrayIndex, winner.getTimeToLeave());
		}

		return timeResults;
	}


}
